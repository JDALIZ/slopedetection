﻿/*
********************************************************
** All Right Reserved to Joel DEROCHE *
********************************************************
*/
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SlopeDetector))]
[CanEditMultipleObjects]
public class SlopeDetectorEditor : Editor
{


  // Use this for initialization
  void OnEnable()
  {

  }

  /// <summary>
  /// 
  /// </summary>
  public void OnSceneGUI()
  {
    var Target = (target as SlopeDetector);
    float AngleToDisplay = Target.SlopeAngle;
    Vector3 DirectionSlope = Target.DirectionSlope;

    if (AngleToDisplay == 0)
    {
      DirectionSlope = Vector3.forward;
    }
    Vector3 b = Target.transform.position - Target.HitFacePosition;
    Vector3 a = ((Target.HitFacePosition + DirectionSlope) - Target.HitFacePosition);
    Vector3 ArcNormal = Vector3.Cross(b, a).normalized;

    Handles.color = Color.yellow;
    Handles.ArrowCap(0, Target.HitFacePosition, Quaternion.FromToRotation(Vector3.forward, DirectionSlope), 2f);
    Vector3 StartVector = new Vector3(DirectionSlope.x,0,DirectionSlope.z).normalized;
    Handles.color = Color.green;
    Handles.DrawSolidArc(Target.HitFacePosition, ArcNormal, StartVector/*Vector3.up*/, Vector3.Angle(StartVector, DirectionSlope), 1f);
    Handles.Label(Target.HitFacePosition , AngleToDisplay.ToString() + "°");
    Handles.color = Color.red;
    Handles.ArrowCap(0, Target.HitFacePosition, Quaternion.FromToRotation(Vector3.forward, StartVector), 2f);
  }

}
